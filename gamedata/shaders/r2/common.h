#ifndef	COMMON_H
#define	COMMON_H

#include "shared\common.h"

#	define USE_NEWGAMMA

// Instead this need: uniform float4		parallax;
#	define PARALLAX_H 0.02
#	define parallax float2(PARALLAX_H, -PARALLAX_H/2)

#	ifndef SMAP_size
#		define SMAP_size	1024
#	endif

#	ifdef	USE_R2_STATIC_SUN
#		define xmaterial half(1.0h/4.h)
#	else
#		define xmaterial half(L_material.w)
#	endif

/* THIS BREAKS DOF
#	define USE_MBLUR
#	define MBLUR_SAMPLES	half(12)
#	define MBLUR_CLAMP		half(0.001)	
#	define MBLUR_WPN_DISABLE
*/

//	#define	LUMINANCE_VECTOR float3(0.299f, 0.587f, 0.114f)
//	#define	LUMINANCE_VECTOR float3(0.25f, 0.25f, 0.2f)
//	#define	LUMINANCE_VECTOR half3(0.3f, 0.38f, 0.22f)
#	define LUMINANCE_VECTOR float3(0.2125, 0.7154, 0.0721)

#	define def_gloss       half(2.f /255.f)
#	define def_aref        half(200.f/255.f)
#	define def_dbumph      half(0.333f)
#	define def_virtualh    half(0.05f)
#	define def_distort     half(0.05f)
#	define def_hdr         half(9.h)
#	define def_hdr_clip	   half(0.75h)
#	define def_lum_hrange  half(0.7h)

#	define SSDO_RADIUS float(0.25)							// radius of ssdo
#	define SSDO_DISCARD_THRESHOLD float(1.5)				// maximum difference in pixel depth. lower value can fix slopes
#	define SSDO_COLOR_BLEEDING float(20.0)					// power of colored shadows. changes overall intensity so use it along with blend factor
#	define SSDO_BLEND_FACTOR float(1.2)						// intensity of shadows

#	define SKY_WITH_DEPTH									// sky renders with depth to avoid some problems with reflections
#	define SKY_DEPTH float(10000.f)
#	define SKY_EPS float(0.001)
#	define FARPLANE float(180.0)

#	define LA_SOFT_SHADOWS
#	define LA_SHADOW_STEPS 2                          		// quantity of lines-transitions (integer)

half3   unpack_normal   (half3 v)   { return 2*v-1; }
half3   unpack_bx2      (half3 v)   { return 2*v-1; }
float3  unpack_bx4      (float3 v)  { return 4*v-2; } //!reduce the amount of stretching from 4*v-2 and increase precision

float2  unpack_tc_base  (float2 tc, float du, float dv) {
    return (tc.xy + float2 (du,dv))*(32.f/32768.f); //!Increase from 32bit to 64bit floating point
}

float2  unpack_tc_lmap  (half2 tc) { return tc*(1.f/32768.f); } // [-1  .. +1 ] 

float   calc_cyclic     (float x) {
    float   phase   = 1/(2*3.141592653589f);
    float   sqrt2   = 1.4142136f;
    float   sqrt2m2 = 2.8284271f;
    float   f       = sqrt2m2*frac(x)-sqrt2;
    return  f*f - 1.f;
}

float2  calc_xz_wave    (float2 dir2D, float frac) {
    // Beizer
    float2  ctrl_A  = float2(0.f, 0.f);
    float2  ctrl_B  = float2(dir2D.x, dir2D.y);
    return  lerp    (ctrl_A, ctrl_B, frac);         //!This calculates tree wave. No changes made
}


half          calc_fogging               (half4 w_pos)      { return dot(w_pos,fog_plane);         }
half2         calc_detail                (half3 w_pos)      {
        float                 dtl        = distance                (w_pos,eye_position)*dt_params.w;
                              dtl        = min              (dtl*dtl, 1);
        half                  dt_mul     = 1  - dtl;        // dt*  [1 ..  0 ]
        half                  dt_add     = .5 * dtl;        // dt+  [0 .. 0.5]
        return                half2      (dt_mul,dt_add);
}
float3         calc_reflection     (float3 pos_w, float3 norm_w)
{
    return reflect(normalize(pos_w-eye_position), norm_w);
}

	float3        calc_sun_r1                (float3 norm_w)    {return L_sun_color*saturate(dot((norm_w),-L_sun_dir_w));}
	float3        calc_model_hemi_r1         (float3 norm_w)    {return max(0,norm_w.y)*L_hemi_color;}
	float3        calc_model_lq_lighting     (float3 norm_w)    {return L_material.x*calc_model_hemi_r1(norm_w) + L_ambient + L_material.y*calc_sun_r1(norm_w);}

//////////////////////////////////////////////////////////////////////////////////////////
struct         v_static                {
        float4      P                	: POSITION;                        // (float,float,float,1)
        float4      Nh                	: NORMAL;                // (nx,ny,nz,hemi occlusion)
        float4      T                 	: TANGENT;                // tangent
        float4      B                 	: BINORMAL;                // binormal
        float2      tc                	: TEXCOORD0;        // (u,v)
        float2      lmh                	: TEXCOORD1;        // (lmu,lmv)
        float4      color               : COLOR0;                        // (r,g,b,dir-occlusion)
};

struct         v_tree               	{
        float4      P                	: POSITION;                // (float,float,float,1)
        float4      Nh                	: NORMAL;                // (nx,ny,nz)
        float3      T                 	: TANGENT;                // tangent
        float3      B                 	: BINORMAL;                // binormal
        float4      tc                	: TEXCOORD0;        // (u,v,frac,???)
};

struct         v_model                 	{
        float4      P                   : POSITION;                // (float,float,float,1)
        float3      N                	: NORMAL;                // (nx,ny,nz)
        float3      T                	: TANGENT;                // (nx,ny,nz)
        float3      B                	: BINORMAL;                // (nx,ny,nz)
        float2      tc                	: TEXCOORD0;        // (u,v)
};

struct        v_detail                    {
        float4      pos                : POSITION;                // (float,float,float,1)
        int4        misc        : TEXCOORD0;        // (u(Q),v(Q),frac,matrix-id)
};

#ifdef  USE_HWSMAP
struct         v_shadow_direct_aref
{
        float4      hpos:        POSITION;       // Clip-space position         (for rasterization)
        float2      tc0:        TEXCOORD1;       // Diffuse map for aref
};
struct         v_shadow_direct
{
        float4      hpos:        POSITION;       // Clip-space position         (for rasterization)
};
#else
struct         v_shadow_direct_aref
{
        float4      hpos:        POSITION;       // Clip-space position         (for rasterization)
        float       depth:         TEXCOORD0;     // Depth
        float2      tc0:        TEXCOORD1;       // Diffuse map for aref
};
struct         v_shadow_direct
{
        float4      hpos:        POSITION;       // Clip-space position         (for rasterization)
        float       depth:         TEXCOORD0;     // Depth
};


#endif

//////////////////////////////////////////////////////////////////////////////////////////
struct         p_bumped        {
        float4      hpos        : POSITION;
#if defined(USE_R2_STATIC_SUN) && !defined(USE_LM_HEMI)
        float4            tcdh        : TEXCOORD0;        // Texture coordinates,         w=sun_occlusion
#else
        float2            tcdh        : TEXCOORD0;        // Texture coordinates
#endif
        float4      position        : TEXCOORD1;        // position + hemi
        half3       M1                : TEXCOORD2;        // nmap 2 eye - 1
        half3       M2                : TEXCOORD3;        // nmap 2 eye - 2
        half3       M3                : TEXCOORD4;        // nmap 2 eye - 3
#ifdef USE_TDETAIL
        float2      tcdbump     	: TEXCOORD5;        // d-bump
    #ifdef USE_LM_HEMI
        float2      lmh             : TEXCOORD6;        // lm-hemi
    #endif
#else
    #ifdef USE_LM_HEMI
        float2      lmh             : TEXCOORD5;        // lm-hemi
    #endif
#endif
};
//////////////////////////////////////////////////////////////////////////////////////////
struct         p_flat                  {
        float4                 hpos        : POSITION;
#if defined(USE_R2_STATIC_SUN) && !defined(USE_LM_HEMI)
    float4                    tcdh        : TEXCOORD0;        // Texture coordinates,         w=sun_occlusion
#else
    float2                    tcdh        : TEXCOORD0;        // Texture coordinates
#endif
        float4                position        : TEXCOORD1;        // position + hemi
        half3                N                : TEXCOORD2;        // Eye-space normal        (for lighting)
  #ifdef USE_TDETAIL
        float2                tcdbump                : TEXCOORD3;        // d-bump
    #ifdef USE_LM_HEMI
        float2         lmh      : TEXCOORD4;        // lm-hemi
    #endif
  #else
    #ifdef USE_LM_HEMI
        float2         lmh      : TEXCOORD3;        // lm-hemi
    #endif
  #endif
};
//////////////////////////////////////////////////////////////////////////////////////////
struct                  f_deffer        		{
        half4           position        		: COLOR0;        // px,py,pz, m-id
        half4           Ne                		: COLOR1;        // nx,ny,nz, hemi
        half4       	C                		: COLOR2;        // r, g, b,  gloss
};
//////////////////////////////////////////////////////////////////////////////////////////
struct  				p_screen                {
        float4          hpos               		: POSITION;
        float2          tc0                		: TEXCOORD0;        // Texture coordinates (for sampling maps)
};
//////////////////////////////////////////////////////////////////////////////////////////
void	tonemap	(out half4 low, out half4 high, half3 rgb, half scale)
{
        rgb	= rgb*scale;

		const float fWhiteIntensity = 1.7;

		const float fWhiteIntensitySQR = fWhiteIntensity*fWhiteIntensity;

#ifdef	USE_BRANCHING
		low = ((rgb*(1+rgb/fWhiteIntensitySQR))/(rgb+1)).xyzz;
        high = low/def_hdr; // 8x dynamic range
#else
        low = half4(((rgb*(1+rgb/fWhiteIntensitySQR))/(rgb+1)),0);
        high = half4(rgb/def_hdr,0); // 8x dynamic range
#endif

}
half4	combine_bloom(half3  low, half4 high)
{
    return half4(low + high*high, 1.h);
}

float3	v_hemi        	(float3 n)                        	{return L_hemi_color*(.5f + .5f*n.y);}
float3	v_hemi_wrap     (float3 n, float w)                	{return L_hemi_color*(w + (1-w)*n.y);}
float3	v_sun           (float3 n)                        	{return L_sun_color*dot(n,-L_sun_dir_w);}
float3	v_sun_wrap      (float3 n, float w)                	{return L_sun_color*(w+(1-w)*dot(n,-L_sun_dir_w));}
half3   p_hemi          (float2 tc)                         {half4 t_lmh = tex2D (s_hemi, tc); return t_lmh.a;}

half   get_hemi( half4 lmh)
{
	return lmh.a;
}

half   get_sun( half4 lmh)
{
	return lmh.g;
}

//	contrast function
half Contrast(half Input, half ContrastPower)
{
     //piecewise contrast function
     bool IsAboveHalf = Input > 0.5 ;
     half ToRaise = saturate(2*(IsAboveHalf ? 1-Input : Input));
     half Output = 0.5*pow(ToRaise, ContrastPower);
     Output = IsAboveHalf ? 1-Output : Output;
     return Output;
}

float4 proj_to_screen(float4 proj)
{
	float4 screen = proj;
	screen.x = (proj.x + proj.w);
	screen.y = (proj.w - proj.y);
	screen.xy *= 0.5;
	return screen;
}

#ifndef SKY_WITH_DEPTH
float is_sky(float depth)
{
	return step(depth, SKY_EPS);
}
float is_not_sky(float depth)
{
	return step(SKY_EPS, depth);
}
#else
float is_sky(float depth)
{
	return step(abs(depth - SKY_DEPTH), SKY_EPS);
}
float is_not_sky(float depth)
{
	return step(SKY_EPS, abs(depth - SKY_DEPTH));
}
#endif


#define FXPS technique _render{pass _code{PixelShader=compile ps_3_0 main();}}
#define FXVS technique _render{pass _code{VertexShader=compile vs_3_0 main();}}

#endif
