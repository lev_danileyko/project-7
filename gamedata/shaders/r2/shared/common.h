//////////////////////////////////////////////////
//     THIS FILE ONLY FOR UNIFORM DEFINES !
/////////////////////////////////////////////////
#ifndef SHARED_COMMON_H
#define SHARED_COMMON_H
uniform half3x4		m_W;
uniform half3x4 	m_V;
uniform half4x4 	m_P;
uniform half3x4 	m_WV;
uniform half4x4 	m_VP;
uniform half4x4 	m_WVP;
uniform float4x4 	m_texgen;
uniform float4x4 	mVPTexgen;
uniform half4   	timers;
uniform half4   	fog_plane;
uniform float4  	fog_params;				// x=near*(1/(far-near)), ?,?, w = -1/(far-near)
uniform half4   	fog_color;
uniform float3  	L_sun_color;
uniform half3   	L_sun_dir_w;
uniform half3   	L_sun_dir_e;
uniform half4   	L_hemi_color;
uniform half4   	L_ambient;				// L_ambient.w = skynbox-lerp-factor
uniform float3  	eye_position;
uniform half3   	eye_direction;
uniform half3   	eye_normal;
uniform float4  	dt_params;
uniform half4   	hemi_cube_pos_faces;
uniform half4   	hemi_cube_neg_faces;
uniform half4   	L_material;				// 0,0,0,mid
uniform half4   	Ldynamic_color;			// dynamic light color (rgb1)    - spot/point
uniform half4   	Ldynamic_pos;			// dynamic light pos+1/range(w) - spot/point
uniform half4   	Ldynamic_dir;			// dynamic light direction - sun

uniform sampler2D   s_base;
uniform sampler2D   s_bump;
uniform sampler2D   s_bumpX;
uniform sampler2D   s_detail;
uniform sampler2D   s_detailBump;
uniform sampler2D   s_detailBumpX;			// Error for bump detail
uniform sampler2D   s_bumpD;
uniform sampler2D   s_hemi;
uniform sampler2D   s_mask;
uniform sampler2D   s_dt_r;
uniform sampler2D   s_dt_g;
uniform sampler2D   s_dt_b;
uniform sampler2D   s_dt_a;
uniform sampler2D   s_dn_r;
uniform sampler2D   s_dn_g;
uniform sampler2D   s_dn_b;
uniform sampler2D   s_dn_a;

// Lighting/shadowing phase
uniform sampler2D   s_depth;
uniform sampler2D   s_position;
uniform sampler2D   s_normal;
uniform sampler     s_lmap;					// 2D/cube projector lightmap
uniform sampler3D   s_material;
uniform sampler1D   s_attenuate;

// Combine phase
uniform sampler2D   s_diffuse;				// rgb.a = diffuse.gloss
uniform sampler2D   s_accumulator;			// rgb.a = diffuse.specular
uniform sampler2D   s_generic;
uniform sampler2D   s_bloom;
uniform sampler     s_image;				// used in various post-processing
uniform sampler2D	s_tonemap;

#endif
