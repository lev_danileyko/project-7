#ifndef SSFX_H
#define SSFX_H

////////////////////////////////////////////////////////////////////////////////////
/// DEFINES
////////////////////////////////////////////////////////////////////////////////////

#define USE_TECHNICOLOR 	 	1
#define USE_DPX					1
#define USE_REINHARDLINEAR		1

////////////////////////////////////////////////////////////////////////////////////
/// TECHNICOLOR
////////////////////////////////////////////////////////////////////////////////////

#define Technicolor2_Red_Strength		0.4		//[0.05 to 1.0] Color Strength of Red channel. Higher means darker and more intense colors.	
#define Technicolor2_Green_Strength		0.4		//[0.05 to 1.0] Color Strength of Green channel. Higher means darker and more intense colors.
#define Technicolor2_Blue_Strength		0.4		//[0.05 to 1.0] Color Strength of Blue channel. Higher means darker and more intense colors.
#define Technicolor2_Brightness			1.0		//[0.5 to 1.5] Brightness Adjustment, higher means brighter image.
#define Technicolor2_Strength			1.0		//[0.0 to 1.0] Strength of Technicolor effect. 0.0 means original image.
#define Technicolor2_Saturation			0.7		//[0.0 to 1.5] Additional saturation control since technicolor tends to oversaturate the image.

////////////////////////////////////////////////////////////////////////////////////
/// DPX
////////////////////////////////////////////////////////////////////////////////////

#define Red   12.0			//[1.0 to 15.0]
#define Green 8.0			//[1.0 to 15.0]
#define Blue  8.0			//[1.0 to 15.0]

#define ColorGamma    2.5	//[0.1 to 2.5] Adjusts the colorfulness of the effect in a manner similar to Vibrance. 1.0 is neutral.
#define DPXSaturation 1.0	//[0.0 to 8.0] Adjust saturation of the effect. 1.0 is neutral.

#define RedC   0.36			//[0.60 to 0.20]
#define GreenC 0.36			//[0.60 to 0.20]
#define BlueC  0.34			//[0.60 to 0.20]

#define Blend 0.2			//[0.00 to 1.00] How strong the effect should be.

////////////////////////////////////////////////////////////////////////////////////
/// ReinhardLinear
////////////////////////////////////////////////////////////////////////////////////

#define ReinhardLinearWhitepoint	3.0
#define ReinhardLinearPoint			0.15
#define ReinhardLinearSlope			2.00 	//[1.0 to 5.0]

#endif